package in.sunilpaulmathew.izzyondroid.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.adapters.ImageViewAdapter;
import in.sunilpaulmathew.izzyondroid.utils.Common;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class ImageViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_layout);

        ViewPager mViewPager = findViewById(R.id.view_pager);

        ImageViewAdapter mAdapter = new ImageViewAdapter();
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(Common.getScreenshotPosition());
    }

}