package in.sunilpaulmathew.izzyondroid.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;

import java.util.ArrayList;

import in.sunilpaulmathew.izzyondroid.BuildConfig;
import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.adapters.SettingsAdapter;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.sCommon.Utils.sSerializableItems;
import in.sunilpaulmathew.sCommon.Utils.sThemeUtils;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class SettingsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_settings, container, false);

        MaterialTextView mCopyright = mRootView.findViewById(R.id.copyright);
        MaterialTextView mVersion = mRootView.findViewById(R.id.version);
        MaterialTextView mIzzy = mRootView.findViewById(R.id.izzy_credit);
        RecyclerView mRecyclerView = mRootView.findViewById(R.id.recycler_view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(requireActivity(), DividerItemDecoration.VERTICAL));
        SettingsAdapter mRecycleViewAdapter = new SettingsAdapter(getData());
        mRecyclerView.setAdapter(mRecycleViewAdapter);

        mCopyright.setText(getString(R.string.copyright, "2021-2022, sunilpaulmathew"));
        mVersion.setText(getString(R.string.version, BuildConfig.VERSION_NAME));

        mIzzy.setOnClickListener(v -> sUtils.launchUrl("https://apt.izzysoft.de/fdroid/repo", requireActivity()));

        return mRootView;
    }

    @SuppressLint("StringFormatMatches")
    private ArrayList <sSerializableItems> getData() {
        ArrayList <sSerializableItems> mData = new ArrayList<>();
        mData.add(new sSerializableItems(sUtils.getDrawable(R.drawable.ic_theme, requireActivity()), getString(R.string.app_theme),
                sThemeUtils.getAppTheme(requireActivity()), null));
        mData.add(new sSerializableItems(sUtils.getDrawable(R.drawable.ic_update, requireActivity()), getString(R.string.update_check_interval),
                getString(R.string.update_check_interval_summary, sUtils.getInt("updateInterval", 24, requireActivity())), null));
        mData.add(new sSerializableItems(sUtils.getDrawable(R.drawable.ic_update, requireActivity()), getString(R.string.latest_timeframe),
                getString(R.string.update_check_interval_summary, sUtils.getLong("latestTimeFrame", 240, requireActivity())), null));
        mData.add(new sSerializableItems(sUtils.getDrawable(R.drawable.ic_refresh, requireActivity()), getString(R.string.refresh_repo),
                getString(R.string.refresh_repo_summary, Common.getAdjustedDate(sUtils.getLong("ucTimeStamp", 0, requireActivity()))), null));
        mData.add(new sSerializableItems(sUtils.getDrawable(R.drawable.ic_gitlab, requireActivity()), getString(R.string.source_code),
                getString(R.string.source_code_summary), "https://gitlab.com/sunilpaulmathew/izzyondroid"));
        mData.add(new sSerializableItems(sUtils.getDrawable(R.drawable.ic_translate, requireActivity()), getString(R.string.translations),
                getString(R.string.translations), null));
        mData.add(new sSerializableItems(sUtils.getDrawable(R.drawable.ic_donation, requireActivity()), getString(R.string.donations),
                getString(R.string.donations_Summary), "https://www.paypal.me/menacherry"));
        return mData;
    }

}