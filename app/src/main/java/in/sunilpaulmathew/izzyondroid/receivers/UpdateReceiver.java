package in.sunilpaulmathew.izzyondroid.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import in.sunilpaulmathew.izzyondroid.utils.AppSettings;
import in.sunilpaulmathew.izzyondroid.utils.PackageData;
import in.sunilpaulmathew.sCommon.Utils.sExecutor;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on June 18, 2022
 */
public class UpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // Update repo information
        new sExecutor() {

            @Override
            public void onPreExecute() {
            }

            @Override
            public void doInBackground() {
                PackageData.acquireRepoData(false, context);
            }

            @Override
            public void onPostExecute() {
                AppSettings.showUpdateNotification(context);
            }
        }.execute();
    }
    
}